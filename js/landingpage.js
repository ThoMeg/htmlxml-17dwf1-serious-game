

function onButtonClick() {
    alert("Hallo lieber Gegenüber. Hier einmal die Regeln des Spiels:\nes werden dir Bilder gezeigt, die du unter die Lupe nehmen musst!" +
        "\nFinde die Fehler, die die Hauptfigur macht und klicke auf diese in den Bildern.\nNur so kannst du das Spiel gewinnen!\n\nViel Spaß");

    let url = new URL('/', window.location.href);
    let params = new URLSearchParams(url.search.slice(1));

    params.append('username', $("#fname").val());
    url.search = params.toString();
    window.location.assign(url.href);

}
