﻿/* *******************************************************
 * Start
 ********************************************************/
'use strict';

function start() {
    //alert("Hello World")
    console.log("Start gestartet!");
    var gameClass = new game();
    console.log("1: " + gameClass);

}

/* *******************************************************
 * Hilfsklassen 
 ********************************************************/

/** @class Erstellt Enumerationen
 */
class Enumeration {
    /**
     * Erstellt eine Instanz einer Enumeration
     * @param obj
     * @returns {Readonly<Enumeration>}
     */
    constructor(obj) {
        for (const key in obj) {
            this[key] = obj[key];
        }
        //freeze, sodass nicht durch Code änderbar
        console.log(this);
        return Object.freeze(this);
    }

    has(key) {
        return this.hasOwnProperty(key);
    }
}

class Chapter {
    constructor() {
        this._chapterID = "ID1"; //StartID
        this._answers = {
            _id: [0, 1, 2, 3],
            _answerText: [],
            _answerIsCorrect: []
        };
        this._image = {
            _imagePath: "",
            _xCoord: 0,
            _xSize: 0,
            _yCoord: 0,
            _ySize: 0,
            _imageObj: 0
        };
        this._questionText = "";
        this._introText = "";
        this._outroText = "";
        this._hintText = "";
        return (this);
    }
}


class User {
    /**
     * User Konstruktor
     * @param userName Nutzername
     * @returns {User}
     */
    constructor(userName) {
        this._userName = userName.toString();
        this._userScore = 0;
        this._userCorrectClicks = 0;
        this._userCountClicks = 0;
        return (this);
    }
}

/* *******************************************************
 * Hilfsfunktionen
 ********************************************************/
const privateMethods = {
    /**
     * Holt die Koordinaten des letzten Klicks
     * @param {number} argArray [0]: X-Position [1]: Y-Position
     * @param currentPicture Das aktuell angezeigte Bild
     * @return {number[]} Array mit Clickpositionen (x, y)
     */
    _getRelativePosition(argArray, currentPicture) {
        let x = argArray[0];
        let y = argArray[1];
        let widthRatio = currentPicture.clientWidth / currentPicture.naturalWidth;
        let heightRatio = currentPicture.clientHeight / currentPicture.naturalHeight;
        x /= widthRatio;
        y /= heightRatio;
        return [x, y];
    },

    /**
     * Prüfen, ob Koordinaten im korrekten Bereich für die Frage sind
     * @param {number} argArray Array mit Koordinaten
     * @param {string} xMin minimaler X-Wert der richtigen Lösung, aus XML
     * @param {string} xRange Breite des Fensters der richtigen Lösung, aus XML
     * @param {string} yMin minimaler Y-Wert der richtigen Lösung, aus XML
     * @param {string} yRange Höhe des Fensters der richtigen Lösung, aus XML
     * @returns {Boolean} richtige Koordinaten?
     */
    _checkCoordinates(argArray, xMin, xRange, yMin, yRange) {
        //dummy 
        var x = argArray[0];
        var y = argArray[1];
        xMin = parseInt(xMin);
        xRange = parseInt(xRange);
        yMin = parseInt(yMin);
        yRange = parseInt(yRange);
        console.log("xmin: " + xMin + "xRange: " + xRange + "ymin: " + yMin + "yRange: " + yRange + "x " + x + "y " + y);
        if ((x <= xMin + xRange && x >= xMin) && (y <= yMin + yRange && y >= yMin)) {
            console.log("Koordinaten geprüft: Korrekt!");
            return true;
        } else {
            console.log("Koordinaten geprüft: Falsch!");
            return false;
        }
    },

    /**
     * Liest XML Datei ein
     * @returns {Object} XML Datei
     * @private
     */
    _fetchInitData() {
        var xml;
        $.ajax({
            url: 'data/xmlfile.xml',
            async: false,
            success: function (response) {
                xml = response;
            }
        });
        return xml;
    },

    /**
     * Kapiteldaten des nächsten Kapitels lesen aus XML
     * @param {Object} currentChapter ChapterObjekt der GameInstanz
     * @param {type} xml Eingelesene XML Datei
     * @returns {Object} Objekt mit Daten des nächsten Kapitels
     */
    _fetchNewChapterData(currentChapter, xml) {
        //aktuelles Kapitel ermitteln, um folgendes zu bestimmen
        var _nextChapterNumber = parseInt(currentChapter._chapterID.charAt(2)) - 1;

        //KapitelID -"ID" --> _currentChapter._chapterID
        var chapter = $(xml).find("chapter")[_nextChapterNumber];
        currentChapter._chapterID = chapter.getElementsByTagName("chapterID")[0].childNodes[0].data;

        //introText --> _currentchapter._introText
        currentChapter._introText = chapter.getElementsByTagName("introText")[0].childNodes[0].data;
        //outroText --> _currentchapter._outroText
        currentChapter._outroText = chapter.getElementsByTagName("outroText")[0].childNodes[0].data;
        //questionText --> _currentchapter._questionText
        currentChapter._questionText = chapter.getElementsByTagName("questionText")[0].childNodes[0].data;
        //hintText --> _currentChapter._hintText
        currentChapter._hintText = chapter.getElementsByTagName("hintText")[0].childNodes[0].data;
        //answers --> _currentchapter._answers{}
        for (let i = 0; i <= chapter.getElementsByTagName("answers").length - 1; i++) {
            currentChapter._answers._id[i] =
                chapter.getElementsByTagName("answers")[i].childNodes[1].innerHTML; //answerID
            currentChapter._answers._answerIsCorrect[i] =
                chapter.getElementsByTagName("answers")[i].childNodes[3].innerHTML; //isCorrect
            currentChapter._answers._answerText[i] =
                chapter.getElementsByTagName("answers")[i].childNodes[5].innerHTML; //answerText
        }
        //image --> _currentchapter._image{}
        currentChapter._image._imagePath =
            chapter.getElementsByTagName("image")[0].childNodes[1].innerHTML; //imagePath
        currentChapter._image._xCoord =
            chapter.getElementsByTagName("image")[0].childNodes[3].innerHTML; //xCoordinates
        currentChapter._image._xSize =
            chapter.getElementsByTagName("image")[0].childNodes[5].innerHTML; //xSize
        currentChapter._image._yCoord =
            chapter.getElementsByTagName("image")[0].childNodes[7].innerHTML; //yCoordinates
        currentChapter._image._ySize =
            chapter.getElementsByTagName("image")[0].childNodes[9].innerHTML; //ySize

        //Chapterzähler hochsetzen
        currentChapter._chapterID = "ID" + (parseInt(currentChapter._chapterID.charAt(2)) + 1);
        //Rückgabe
        console.log(currentChapter);
        return currentChapter;
    },

    /**
     * blendet Textelemete im HTML Doc ein
     * @param {type} element Textelement auf HTMLDokument
     * @param {type} text Zu schreibender Text
     */
    _showText(element, text) {
        //element.font = "30px Arial";
        console.log("_showText: " + element + " " + text);
        $(element).toggle(true);
        element.innerHTML = text;
    },

    /**
     * blendet Textelemente im HTML Doc aus
     * @param element
     * @private
     */
    _hideText(element) {
        console.log("_hideText: " + element);
        element.innerHTML = "";
        $(element).toggle(false);
    },

    /**
     * Blendet Array an Buttons ein. Setzt Labeltexte
     * @param buttons ButtonArray
     * @param answers AntwortenObjekt [_answerIsCorrect[], _answerText[]]
     * @private
     */
    _showButtons(buttons, answers) {
        for (const [i, button] of buttons.entries()) {
            //Iteration über Array von Buttons, zeige diese und zugehörige Buttons
            console.log("_showButtons: " + i + " " + button);
            button.labels[0].innerHTML = answers._answerText[i];
            button.value = answers._answerIsCorrect[i];
            $(button).toggle(true);
            $("label[for=" + button.id + "]").toggle(true);
        }
    },

    /**
     * Versteckt Antwortauswahl-Radiobuttons
     * @param buttons
     * @private
     */
    _hideButtons(buttons) {
        for (const [i, value] of buttons.entries()) {
            //Iteration über Array von Buttons, Verstecke diese und zugehörige Labels
            console.log("_hideButtons: " + i + " " + value);
            $(value).prop('checked', false);
            $(value).toggle(false);
            $("label[for=" + value.id + "]").toggle(false);
        }
    },

    _move(currChapter, maxChapter) {
        var elem = document.getElementById("myBar");
        var width = 5;
        if (currChapter > 1) {
            width = Math.round((currChapter - 1) / maxChapter * 100);
        }
        var id = setInterval(frame, 10);

        function frame() {
            let progress = Math.round(currChapter / maxChapter * 100);
            if (width === progress) {
                clearInterval(id);
            } else {
                width++;
                elem.style.width = width + '%';
                elem.innerHTML = width + '%';
            }
        }
    },

    /**
     * erhöht Punktzahl um Wert
     * @private
     */
    _updateDatabaseScore(user) {
        let exportUsername = user._userName;
        let exportScore = user._userScore;
        let exportAccuracy = Math.round(user._userCorrectClicks / user._userCountClicks * 100);
        $.ajax({
            type: "POST",
            url: 'dbpost.php',
            data: {
                functionname: 'insert', arguments: {userName: exportUsername, score: exportScore, accuracy: exportAccuracy}
            },
            success: function(result){
                console.log("Erfolg! " + result);
            },
            error: function (jqxhr, status, exception) {
                console.log("Ausnahme: " + exception);
            }
        });
    },

    /**
     * blendet Punkte ein
     * @param user
     * @private
     */
    _showScore(user) {
        document.getElementById("scoreText").innerHTML = user._userName + ", deine Punktzahl ist: " + user._userScore + "\nDeine Genauigkeit: "
            + user._userCorrectClicks + "/" + user._userCountClicks + ".";
        $(document.getElementById("scoreText")).toggle();

        let exportUsername = user._userName;
        let exportScore = user._userScore;
        let exportAccuracy = Math.round(user._userCorrectClicks / user._userCountClicks * 100);
        $.redirect('highscore.php', { arguments: {userName: exportUsername, score: exportScore, accuracy: exportAccuracy}});
    },

    _getMaxChapters(xml) {
        //Annahme: Chapter in der "richtigen" Reihenfolge
        //return $(xml).find("chapter").last().find("chapterID").text();
        //FIX: Off by one Error bei maximaler Chapteranzahl
        //Abschneiden der ID Nummer des letzten kapitels, dann eins drauf rechnen.
        let maxChapters = $(xml).find("chapter").last().find("chapterID").text().slice(2);
        maxChapters = "ID" + (parseInt(maxChapters) + 1);
        return maxChapters;
    }
};


class game {
    _stateEnum; //Enum an potentiellen Zuständen
    _slideshow; //Canvas Element auf HTML Seite
    _docQuestionText; //QuestionText Bereich auf der HTML Seite
    _docHintText; //HintText Bereich auf der HTML Seite
    _docIntroText; //IntroText Bereich auf der HTML Seite
    _docOutroText; //OutroText Bereich auf der HTML Seite
    _docAnswerButtons; //AntwortButtons auf der HTML Seite
    _errorCounter; //Counter für falsche Klicks
    _currentChapter; //Aktuelles Kapitel, Spielfortschritt, vgl Klasse Chapter
    _state; //Aktueller Zustand des Spiels
    _isClickCorrect; //letzter Klick mit richtigen Koordinaten?
    _user; //Nutzer, [Name, Score]
    _answer; //letzte Antwort auf Frage
    _maxChapters; //Maximale Anzahl Kapitel, "wann endet das Spiel"
    _xml; //XML Datei für Kapitel Daten

    /** Erstellt eine Instanz game, startet Spiel
     * @constructor
     * */
    constructor() {
        this._currentChapter = new Chapter();

        this._slideIndex = 1;
        this._maxChapters = "ID3";
        let url = new URL(window.location.href);
        let searchparams = new URLSearchParams(url.search.slice(1));
        this._user = new User(searchparams.getAll("username"));

        this._errorCounter = 0;
        this._stateEnum = new Enumeration({
            START: Symbol("start"),
            LOAD_CHAPTER: Symbol("loadChapter"),
            SHOW_PICTURE: Symbol("showPicture"),
            AWAITING_PICTURE_CLICK: Symbol("awaitingPictureClick"),
            HINT: Symbol("hint"),
            SHOW_QUESTION: Symbol("showQuestion"),
            AWAITING_ANSWER: Symbol("awaitingAnswer"),
            EVALUATE_QUESTION: Symbol("evaluateQuestion"),
            SHOW_INFO: Symbol("showInfo"),
            GAME_END: Symbol("gameEnd"),
            SHOW_SCORE: Symbol("showScore"),
            STOP: Symbol("stop")
        });
        this._state = this._stateEnum.START;
        this.stateMashine();
    }

    //"this" bound to game-Instanz (vgl stateMachine(). ()=>this.onSlideshowClick())
    /**
     * onClick Function für Eventlistener
     * @returns {undefined}
     */
    onSlideshowClick() {
        if (this._state === this._stateEnum.AWAITING_PICTURE_CLICK  || this._state === this._stateEnum.HINT) {
            this._isClickCorrect = null;
            //Relative Position innerhalb des Canvas ermitteln, dann prüfen, ob korrekte Antwort
            var argArrayGetPosition = [event.x - this._slideshow.offsetLeft, event.y - this._slideshow.offsetTop];
            var argArrayCoordinatesToCheck = privateMethods._getRelativePosition(argArrayGetPosition, document.getElementsByClassName("mySlides")[this._currentChapter._chapterID.slice(2) - 2] );
            //console.log(argArrayCheckCoordinates);
            let image = this._currentChapter._image;
            this._isClickCorrect =
                privateMethods._checkCoordinates(argArrayCoordinatesToCheck, image._xCoord, image._xSize, image._yCoord, image._ySize);
            if (this._isClickCorrect)
                this._user._userCorrectClicks++;
            this._user._userCountClicks++;
            console.log(this._isClickCorrect);
        }
        console.log(this._state);
        this.stateMashine();
    }

    plusDivs(n) {
        this._slideIndex = this._slideIndex + n;
        this.showDivs(this._slideIndex);
    }

    showDivs(n) {
        var x = document.getElementsByClassName("mySlides");
        if (n > x.length) {
            this._slideIndex = 1
        }
        if (n < 1) {
            this._slideIndex = x.length
        }
        for (let i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        x[this._slideIndex - 1].style.display = "block";
    }
    /**
     * Ändert Zustand der GameInstanz, finiter Automat wird somit für nächsten Schritt vorbereitet
     * @param {string} state Zustand, auf den gewechselt wird
     * @returns {undefined}
     */
    setState(state) {
        if (this._stateEnum.has(state)) {
            this._state = this._stateEnum[state];
            console.log("Neuer state: " + this._state.toString());
        } else {
            console.log("setState: ungültiger Zustand! " + state);
        }
    }

    /**
     * Wertet aktuellen Zustand aus, setzt Spiel entsprechend fort
     * "Motor" des ganzen Gerummels
     * */
    async stateMashine() {
        switch (this._state) {
            //Initiale Prozesse
            case this._stateEnum.START:
                //HTML Elemente von Dokument holen
                //Slideshow
                this._slideshow = document.getElementById("slideshow");
                //FrageText Bereich
                this._docQuestionText = document.getElementById("questionText");
                //AntwortButtons
                this._docAnswerButtons = [];
                for (let i = 1; i <= 4; i++) {
                    let buttonID = "answerButton" + i;
                    this._docAnswerButtons[i - 1] = document.getElementById(buttonID);
                }
                //Antwortbutton
                this._answerSubmitButton = document.getElementById("answerSubmitButton");
                this._answerSubmitButton.addEventListener("click", () => {
                    if (this._state === this._stateEnum.AWAITING_ANSWER) {
                        if ($("input[name='gender']").is(':checked')){ //Ist eine Antwort ausgewählt?
                            this.setState("EVALUATE_QUESTION");
                            this._answer = $("input[name='gender']:checked").val();
                        } else {
                            alert("Bitte wählen sie eine Antwort aus!");
                            this.setState("AWAITING_ANSWER");
                        }
                    }
                    $(this._slideshow).trigger("click");
                });
                //HintText
                this._docHintText = document.getElementById("hintText");
                //IntroText
                this._docIntroText = document.getElementById("introText");
                //OutroText
                //this._docOutroText = document.getElementById("outroText");

                //XML auslesen
                this._xml = privateMethods._fetchInitData();
                this._maxChapters = privateMethods._getMaxChapters(this._xml);

                console.log(this._xml);

                //KlickEventListener hinzufügen
                $(document).off().on('click', '#slideshow', () => this.onSlideshowClick());

                this.setState("LOAD_CHAPTER");
                break;
//***********************************************************************
            //Kapitel laden
            case this._stateEnum.LOAD_CHAPTER:

                this._currentChapter = privateMethods._fetchNewChapterData(this._currentChapter, this._xml);
                this.setState("SHOW_PICTURE");
                //Nächsten Click simulieren
                break;
//***********************************************************************
            //Bild anzeigen
            case this._stateEnum.SHOW_PICTURE:

                let imageLoaded = new Promise(resolve => {
                    let img = new Image();
                    //Warten, bis image geladen ist
                    img.addEventListener('load', () => {
                        //Bild der Slideshow hinzufügen
                        let appendedHTML = '<img class="mySlides w3-animate-opacity" src=' + img.src + ' style="width:100%; display:none" alt="hier sollte ihr Bild sein...">';
                        $('#slideshow').append(appendedHTML);
                        this.plusDivs(1);
                        privateMethods._showText(this._docIntroText, this._currentChapter._introText);
                        this.setState("AWAITING_PICTURE_CLICK");
                        resolve();
                    });
                    img.src = "images/" + this._currentChapter._image._imagePath;
                    this._currentChapter._image._imageObj = img;
                });

                await Promise.resolve(imageLoaded);
                break;

//***********************************************************************
            //Warten auf Klick auf Bild
            case this._stateEnum.AWAITING_PICTURE_CLICK:

                if (this._isClickCorrect) {
                    this.setState("SHOW_QUESTION");
                    this._errorCounter = 0;
                } else {
                    this._errorCounter++;
                    if (this._errorCounter === 3) {
                        privateMethods._showText(this._docHintText, this._currentChapter._hintText);
                        this.setState("HINT");
                    }
                }
                break;

//***********************************************************************
            //Hinweis angezeit
            case this._stateEnum.HINT:
                if (this._isClickCorrect) {
                    this.setState("SHOW_QUESTION");
                    this._errorCounter = 0;
                    privateMethods._hideText(this._docHintText);
                }

                break;

//***********************************************************************
            //Frage einblenden
            case this._stateEnum.SHOW_QUESTION:
                privateMethods._showText(this._docQuestionText, this._currentChapter._questionText);
                privateMethods._showButtons(this._docAnswerButtons, this._currentChapter._answers);
                $(this._answerSubmitButton).toggle(true);
                this.setState("AWAITING_ANSWER");
                break;

//***********************************************************************
            //Warten auf Antwortauswahl
            case this._stateEnum.AWAITING_ANSWER:
                //siehe Button onClick Event
                break;

//***********************************************************************
            //Frage auswerten
            case this._stateEnum.EVALUATE_QUESTION:
                console.log("aktuell: " + this._currentChapter._chapterID + "max: " + this._maxChapters);
                if (this._answer === "true") { //Abfrage auf String, da Wert aus XML als String eingelesen wird
                    //richtige Antwort
                    this._user._userScore++;
                    this.setState("SHOW_INFO");
                } else {
                    //falsche Antwort
                    this._currentChapter._outroText = "FALSCH! \n" + this._currentChapter._outroText;
                    this.setState("SHOW_INFO");
                }
                //progressBar fortsetzen
                privateMethods._move((parseInt(this._currentChapter._chapterID.slice(2)) - 1 ), (parseInt(this._maxChapters.slice(2)) - 1));

                //Frage, Antworten und Submit Button wieder verstecken
                privateMethods._hideButtons(this._docAnswerButtons);
                privateMethods._hideText(this._docQuestionText);
                privateMethods._hideText(this._docHintText);
                privateMethods._hideText(this._docIntroText);

                $(this._answerSubmitButton).toggle();
                break;

//***********************************************************************
            //Spielende
            case this._stateEnum.SHOW_INFO:
                alert(this._currentChapter._outroText);
                this.setState("LOAD_CHAPTER");
                if (this._currentChapter._chapterID === this._maxChapters) {
                    this.setState("GAME_END");
                }
                break;

//***********************************************************************
            //Spielende
            case this._stateEnum.GAME_END:
                //Punkte speichern auf Datenbank
                privateMethods._updateDatabaseScore(this._user);
                this.setState("SHOW_SCORE");
                break;

            case this._stateEnum.SHOW_SCORE:
                this.setState("STOP");
                privateMethods._showScore(this._user);

        }
        if (//this._state === this._stateEnum.START ||
            this._state === this._stateEnum.LOAD_CHAPTER ||
            this._state === this._stateEnum.SHOW_PICTURE ||
            this._state === this._stateEnum.SHOW_QUESTION ||
            this._state === this._stateEnum.EVALUATE_QUESTION ||
            this._state === this._stateEnum.SHOW_INFO ||
            this._state === this._stateEnum.SHOW_SCORE ||
            this._state === this._stateEnum.GAME_END) {
            //Nächsten Click simulieren
            //Notwendig, damit Statemachine bei States, die keine Nutzereingabe erfordern, fortgesetzt wird.
            console.log("Tried AutoClick at State " + this._state.toString());
            $(this._slideshow).trigger("click");
        }
    }

}