<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <link rel="stylesheet" href="https://www.w3schools.com/lib/w3-theme-black.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css">
    <title>Dein Ergebnis...</title>
</head>
<body>

<div class="w3-container">
    <hr>
    <div class="w3-center">
        <h2 class="text-white">Highscore</h2>
    </div>
    <div class="w3-responsive w3-card-4">
        <?php
        $all = $_POST['arguments'];
        $name = $all["userName"];
        $score = $all["score"];
        $acc = $all["accuracy"];
        $name = "'" . $name . "'";

        $conn = mysqli_connect('localhost', 'htmlxml17dwf1', '17DWF1!', 'htmlxml17dwf1');

        //
        $sql = "SELECT username, score, accuracy from users order by score desc, accuracy desc ";
        $tableData = mysqli_query($conn, $sql);

        //$conn = new mysqli('localhost', 'htmlxml17dwf1', '17DWF1!', 'htmlxml17dwf1');
        //$sql = "SELECT username, score, accuracy from users order by score, accuracy asc";
        //$tableData = mysqli_query($conn, $sql);
        ?>
        <table class="w3-table w3-striped w3-border">
            <tr>
                <th>Name</th>
                <th>Punkte</th>
                <th>Genauigkeit</th>
            </tr>
            <?php while ($row = $tableData->fetch_assoc()) { ?>
                <tr <?php if ($row["name"] == $name) {
                    echo "class=\"w3-blue\"";
                } ?> >
                    <td><?php echo $row["username"] ?></td>
                    <td><?php echo $row["score"] ?></td>
                    <td><?php echo $row["accuracy"] ?></td>
                </tr>
            <?php } ?>
        </table>
    </div>
</div>
</body>
</html>