# HTMLXML 17DWF1 Serious Game

Serious Game zum Thema Datenschutz im Rahmen der Vorlesung HTML/XML und Usability
Optimalerweise ist die Seite unter http://htmlxml17dwf1.bplaced.net/Landingpage.html erreichbar. 
Sollte dies nicht der Fall sein, stellt dieses Repo die Möglichkeit dar, das Projekt lokal einzurichten.

**Projektmitglieder:**
* Mattes Barkowski (802494)
* Nina Behrensdorf ()
* Steffen Kocks (825090)
* Thomas Megger (809510)
* Kimberly Ostermann (798961)

**Installation:**
* Repo klonen
* In MySQL die `init.sql` ausführen, um
    * die Datenbank mit Namen htmlxml17dwf1 anzulegen 
    * die notwendige Tabelle anzulegen
    * Testdaten einzupflegen
* in den Dateien PHP Dateien `highscore.php` und `dbpost.php` die Zeile 
* `$conn = new mysqli('localhost', 'db_user', 'db_password', 'htmlxml17dwf1');`
* anpassen, sodass db_user und db_password einen Nutzer mit Lese- und Schreiberechten in MySQL darstellen
* Gegebenenfalls 'localhost' gegen die Adresse der MySQL Datenbank austauschen
* die Dateien unter einer Adresse auf dem lokalen Webserver hinterlegen und im Browser die Landingpage aufrufen
 